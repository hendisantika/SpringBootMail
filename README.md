# Spring Boot Mail
## Things to do list
1. Clone this repository: `git clone https://gitlab.com/hendisantika/SpringBootMail.git`.
2. Go to your folder: `cd SpringBootMail`.
3. Set Your own email & password.
4. Run the application: `mvn clean spring-boot:run`
5. Open your favorite browser.

## Screen shot

sendSimpleEmail: http://localhost:8080/sendSimpleEmail

![sendSimpleEmail](img/simple.png "sendSimpleEmail")

sendHtmlEmail: http://localhost:8080/sendHtmlEmail

![sendHtmlEmail](img/html.png "sendHtmlEmail")

sendAttachmentEmail: http://localhost:8080/sendAttachmentEmail

![sendAttachmentEmail](img/attachment.png "sendAttachmentEmail")
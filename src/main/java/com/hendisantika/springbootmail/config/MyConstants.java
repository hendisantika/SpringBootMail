package com.hendisantika.springbootmail.config;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-mail
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/07/18
 * Time: 22.15
 * To change this template use File | Settings | File Templates.
 */
public class MyConstants {
    // Replace with your email here:
    public static final String MY_EMAIL = "yourEmail@gmail.com";

    // Replace password!!
    public static final String MY_PASSWORD = "yourPassword";

    // And receiver!
    public static final String FRIEND_EMAIL = "yourFriend@gmail.com";

}
